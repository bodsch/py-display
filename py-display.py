#!/usr/bin/env python3

from mpd import MPDClient
#from select import select
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
from fonts.ttf import RobotoMedium as UserFont

# import time

import ST7735
#import urllib
#import json
#import os.path
#import pprint

HOST = 'localhost'
PORT = '6600'

class Display:
    """
    """
    def __init__(self):
        self.dc = 24
        self.rst = 25
        self.spi_port = 0
        self.spi_device = 0

        self.display_width = 128
        self.display_height = 160

        self.disp = ST7735.ST7735(
            port = self.spi_port,
            cs = self.spi_device, # ST7735.BG_SPI_CS_FRONT,  # BG_SPI_CSB_BACK or BG_SPI_CS_FRONT
            dc = self.dc,
            rst = self.rst,
            backlight = 18,               # 18 for back BG slot, 19 for front BG slot.
            width = self.display_width,
            height = self.display_height,
            # offset_top = 0,
            # offset_left = 0,
            invert=False,
            # rotation=90,
            # spi_speed_hz=4000000
        )

    def clear(self):
        self.disp.begin()

        color=(0,0,0) # 255,255,255)

        buffer = Image.new('RGB', (self.display_width, self.display_height))

        width, height = buffer.size
        buffer.putdata([color]*(width*height))

        self.disp.display(buffer)

    # Displays data and text on the 0.96" LCD
    def display_text(self, message=""):
        """
        """
        print(f" -> {message}")

        text_color = (255, 255, 255)
        back_color = (0, 0, 0)

        img = Image.new(
            'RGB', 
            (self.display_width, self.display_height), 
            color = back_color
        )
        draw = ImageDraw.Draw(img)

        font_size_small = 10
        font_size_large = 20
        font = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf", font_size_large)

        # font = ImageFont.truetype(UserFont, font_size_large)
        # smallfont = ImageFont.truetype(UserFont, font_size_small)
        x_offset = 2
        y_offset = 2

        draw.rectangle(
            (x_offset, y_offset, self.display_width, self.display_height), 
            back_color
        )
        size_x, size_y = draw.textsize(
            str(message), 
            font=font
        )
        print(f"  {size_x} x {size_y}")
        x = (self.display_width - size_x) / 2
        y = (self.display_height / 2) - (size_y / 2)
        print(f"  {x} x {y}")

        # Write the text at the top in black
        draw.text(
            (x, y),
            str(message),
            font = font,
            fill = text_color
        )
        self.disp.begin()
        
        self.disp.display(img)

class SongNotify:
    def __init__(self):
        """
        
        """
        pass

    def newSong(self, song):
        hello = pynotify.Notification(
                    song.title,
                    song.title + " by " + song.artist + " from album " + song.album, 
                    song.icon
                )
        hello.show()

class Song:
    def __init__(self, song_dict):

        print(song_dict)

        self.id = song_dict["id"]
        if song_dict.get("album"):
            self.album = song_dict.get("album")

        if song_dict.get("artist"):
            self.artist = song_dict["artist"]

        self.title = song_dict["title"]

        self.icon = None

class MpdWatcher:
    """
    """
    def __init__(self, host, port):
        self.client = MPDClient()

        self.display = Display()
        self.display.clear()        

        try:
            self.client.connect(HOST, PORT)
        except SocketError:
            print("Failed to connect to MPD, exiting")
            sys.exit(1)

        self.song = None
        self.updateSong(self.client.currentsong())

    def watch(self):
        while True:
            changed = self.client.idle()
            # print(changed)
            if 'player' in changed:
                self.updateSong(self.client.currentsong())

    def updateSong(self, song):
        print(f"updateSong( {song})")

        if not 'id' in song:
            return

        # if self.song and song['id'] == self.song.id:
        #     return

        self.song = Song(song)

        print(f"  {self.client.status()}")

        if self.client.status()['state'] == 'play':
            print(song.get('title'))
            self.display.display_text(song.get('title'))            

        #    # self.notify.newSong(self.song)
        #else:
        #    print(self.client.status()['state'])

def main():
    """
    """
    watch = MpdWatcher(HOST, PORT)
    watch.watch()

if __name__ == '__main__':
    main()
